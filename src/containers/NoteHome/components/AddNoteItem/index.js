import React from 'react';
import { MdAdd } from 'react-icons/md';
import '../style.less';
import { Link, withRouter } from 'react-router-dom';

const NoteItem = props => {
  return (
    <section className='note-item'>
      <Link to='/notes/create'>
        <MdAdd size='50px'/>
      </Link>
    </section>
  )
};

export default withRouter(NoteItem);