import React from 'react';
import { withRouter } from 'react-router-dom';
import '../style.less';

const NoteItem = props => {
  const goToDetail = () => props.history.push(`/notes/${props.note.id}`);
  return (
    <section onClick={goToDetail} className='note-item'>
      {props.note.title}
    </section>
  )
};

export default withRouter(NoteItem);