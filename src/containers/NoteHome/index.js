import React from 'react';
import { connect } from 'react-redux';
import { getNotes } from './actions'
import NoteItem from './components/NoteItem';
import './style.less';
import AddNoteItem from './components/AddNoteItem';

class NoteHome extends React.Component{

  componentDidMount() {
    this.props.getNotes();
  }

  render() {
    const { notes } = this.props;
    return (
      <section className='note-home'>
        <section className='note-list-wrapper'>
          {notes.map(item => (
            <NoteItem key={item.id} note={item}/>
          ))}
          <AddNoteItem/>
        </section>
      </section>
    )
  }
}

export default connect(state => ({
  notes: state.notes
}), {
  getNotes
})(NoteHome);