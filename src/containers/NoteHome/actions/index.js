import * as Types from '../../../types';
import * as API from '../../../api';

export const getNotes = () => dispatch => {
  API.getNotes().then(res => {
    dispatch({
      type: Types.GET_NOTES,
      notes: res
    })
  })
};