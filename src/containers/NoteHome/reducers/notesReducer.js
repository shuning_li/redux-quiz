import * as Types from '../../../types';
const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_NOTES:
      return action.notes;
    default:
      return state;
  }
}