import React from 'react';
import './style.less';
import Refer from './components/Refer';
import Content from './components/Content/Content';
import {connect} from 'react-redux';
import { getNotes } from '../NoteHome/actions'

class NoteDetail extends React.Component{

  componentDidMount() {
    this.props.getNotes();
  }


  render() {
    const { notes } = this.props;
    const { id } = this.props.match.params;
    const note = notes.find(item => item.id === parseInt(id));
    return (
      <section className='note-detail'>
        <section className='refer-wrapper'>
          <Refer notes={notes}/>
        </section>
        <section className='content-wrapper'>
          {note && <Content note={note}/>}
        </section>
      </section>
    )
  }
}

export default connect(state => ({
  notes: state.notes
}), {
  getNotes
})(NoteDetail);