import React from 'react';
import './style.less';

const Content = props => {
  const { note } = props;
  return (
    <section className='note-content'>
      <section class='title-wrapper'>
        <h1>{note.title}</h1>
      </section>
      <section className='description-wrapper'>
        {note.description}
      </section>
    </section>
  )
};

export default Content;