import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import './style.less';

const Refer = props => {
  const { id } = props.match.params;
  return (
    <section className='note-refer'>
      <ul>
        {props.notes.map(item => (
          <li className={parseInt(id) === item.id ? 'link-active' : ''} key={item.id}>
            <Link to={`/notes/${item.id}`}>{item.title}</Link>
          </li>
        ))}
      </ul>
    </section>
  )
};

export default withRouter(Refer);