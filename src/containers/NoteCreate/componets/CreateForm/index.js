import React from 'react';
import { withRouter } from 'react-router-dom';
import './style.less';

const CreateForm = props => {

  const handleTitleChange = e => {
    props.handleTitleChange(e.target.value);
  };
  const handleDescriptionChange = e => {
    props.handleDescriptionChange(e.target.value);
  };
  const createNote = e => {
    e.preventDefault();
    const note = {title: props.title, description: props.description};
    props.createNote(note);
  };

  const goToHome = e => {
    e.preventDefault();
    props.history.push('/');
  };

  return (
    <form className='create-note-form'>
      <header className='form-header'>
        <h1>创建笔记</h1>
      </header>
      <section>
        <section className='title-input-wrapper'>
          <label htmlFor='title'>
            标题
          </label>
          <input onChange={handleTitleChange} value={props.title} id='title' type="text"/>
        </section>
        <section className='description-input-wrapper'>
          <label htmlFor="description">正文</label>
          <textarea onChange={handleDescriptionChange} value={props.description} id="description" cols="30" rows="10"/>
        </section>
        <section className='button-wrapper'>
          <button disabled={props.title.length === 0 || props.description.length === 0} onClick={createNote}>提交</button>
          <button onClick={goToHome}>取消</button>
        </section>
      </section>
    </form>
  )
};

export default withRouter(CreateForm);