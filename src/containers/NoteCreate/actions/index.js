import * as Types from '../../../types';
import * as API from '../.././../api';

export const handleTitleChange = title => ({
  type: Types.HANDLE_TITLE_CHANGE,
  title
});

export const handleDescriptionChange = description => ({
  type: Types.HANDLE_DESCRIPTION_CHANGE,
  description
});

export const createNote = note => dispatch => {
  API.createNote(note).then(isOk => {
    if (isOk) {
      window.location.replace('/');
      dispatch({
        type: Types.CREATE_NOTE,
        note
      })
    }
  })
};