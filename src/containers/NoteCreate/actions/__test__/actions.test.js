import * as Actions from '../index';
import * as Types from '../../../../types';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

test('should change title input', () => {
  const title = 'hello';
  const expectedAction = {
    type: Types.HANDLE_TITLE_CHANGE,
    title
  };
  expect(Actions.handleTitleChange(title)).toEqual(expectedAction)
});


test('should change description input', () => {
  const description = 'hello';
  const expectedAction = {
    type: Types.HANDLE_DESCRIPTION_CHANGE,
    description
  };
  expect(Actions.handleDescriptionChange(description)).toEqual(expectedAction)
});

// test('should create new note', () => {
//   fetchMock.postOnce('/api/posts', {
//     body: { title: 'note', description: 'hello' },
//     headers: { 'content-type': 'application/json;charset=UTF-' }
//   });
//
//   const expectedAction = { type: Types.CREATE_NOTE, note: { title: 'note', description: 'hello' } };
//   const store = mockStore({ notes: [] })
//
//   return store.dispatch(Actions.createNote({ title: 'note', description: 'hello' })).then(() => {
//     // return of async actions
//     expect(store.getAction()).toEqual(expectedAction)
//   })
// });