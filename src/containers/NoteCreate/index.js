import React from 'react';
import Header from '../../components/header';
import CreateForm from './componets/CreateForm';
import { connect } from 'react-redux';
import { handleDescriptionChange, handleTitleChange, createNote } from './actions';
import './style.less';

class NoteCreate extends React.Component{

  render() {
    const { title, description, handleTitleChange, handleDescriptionChange, createNote } = this.props;
    return (
      <section className="note-create">
        <section className='form-wrapper'>
          <CreateForm title={title}
                      description={description}
                      createNote={createNote}
                      handleTitleChange={handleTitleChange}
                      handleDescriptionChange={handleDescriptionChange} />
        </section>
      </section>
    )
  }

}

export default connect(state => state.form, {
  handleDescriptionChange,
  handleTitleChange,
  createNote
})(NoteCreate);