import formReducer from '../formReducer';
import * as Types from '../../../../types';

test('should change title', () => {
  expect(
    formReducer('', {
      type: Types.HANDLE_TITLE_CHANGE,
      title: 'hello'
    })
  ).toEqual({
    title: 'hello'
  })
});

test('should change description', () => {
  expect(
    formReducer('', {
      type: Types.HANDLE_DESCRIPTION_CHANGE,
      description: 'hello'
    })
  ).toEqual({
    description: 'hello'
  })
});