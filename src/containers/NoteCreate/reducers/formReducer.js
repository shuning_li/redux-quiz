import * as Types from '../../../types';

const initialState = {
  title: '',
  description: ''
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.HANDLE_TITLE_CHANGE:
      return {...state, title: action.title};
    case Types.HANDLE_DESCRIPTION_CHANGE:
      return {...state, description: action.description};
    default:
      return state;
  }
}