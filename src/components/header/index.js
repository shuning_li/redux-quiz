import React from 'react';
import { MdAssignment } from 'react-icons/md'
import './style.less';

const Header = props => {
  return (
    <header className='note-header'>
      <section className='icon-wrapper'>
        <MdAssignment size='28px'/>
      </section>
      <span data-testid='title'>{props.title}</span>
    </header>
  )
};

Header.defaultProps = {
  title: 'NOTES'
};

export default Header;