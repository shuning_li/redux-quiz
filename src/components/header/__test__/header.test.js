
import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render } from '@testing-library/react';
import Header from '../index';
import React from 'react';

test('should render header', () => {
  const { getByTestId }  = render(<Header title='NOTES'/>);
  expect(getByTestId('title')).toHaveTextContent('NOTES');
});