import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.less';
import NoteCreate from './containers/NoteCreate';
import NoteHome from './containers/NoteHome';
import NoteDetail from './containers/NoteDetail';
import Header from './components/header';

class App extends Component {
  render() {
    return (
      <div className='App'>
        <Header/>
        <Router>
          <Switch>
            <Route exact path='/' component={NoteHome} />
            <Route path='/notes/create' component={NoteCreate}/>
            <Route path='/notes/:id' component={NoteDetail}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;