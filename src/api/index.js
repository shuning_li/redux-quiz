const URL = 'http://localhost:8080/api/posts';

export const createNote = data => {
  return fetch(URL, {
    method: 'POST',
    headers: {
      'content-type': 'application/json;charset=UTF-8'
    },
    body: JSON.stringify(data)
  }).then(response => response.ok)
};

export const getNotes = () => {
  return fetch(URL).then(response => response.json());
};