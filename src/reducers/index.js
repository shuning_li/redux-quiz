import {combineReducers} from "redux";
import formReducer from '../containers/NoteCreate/reducers/formReducer';
import notesReducer from '../containers/NoteHome/reducers/notesReducer';

const reducers = combineReducers({
  form: formReducer,
  notes: notesReducer
});
export default reducers;